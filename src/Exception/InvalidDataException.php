<?php
declare(strict_types=1);

namespace App\Exception;


class InvalidDataException extends \Exception
{
    const INVALID_MESSAGE = 'Invalid data';

    public function __construct()
    {
        parent::__construct(self::INVALID_MESSAGE);
    }
}