<?php
declare(strict_types=1);

namespace App\Exception;

class NotFoundException extends \Exception
{
    const NOT_FOUND_MESSAGE= 'Not found';

    public function __construct()
    {
        parent::__construct(self::NOT_FOUND_MESSAGE);
    }
}