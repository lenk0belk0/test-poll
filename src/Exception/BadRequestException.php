<?php
declare(strict_types=1);

namespace App\Exception;

class BadRequestException extends \Exception
{
    const BAD_REQUEST_MESSAGE = 'Bad request';

    public function __construct(string $message = null)
    {
        parent::__construct($message === null ? self::BAD_REQUEST_MESSAGE: $message);
    }
}