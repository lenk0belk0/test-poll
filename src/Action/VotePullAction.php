<?php
declare(strict_types=1);

namespace App\Action;

use App\Entity\Answer;
use App\Entity\Pull;
use App\Entity\Voice;
use App\Exception\BadRequestException;
use App\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VotePullAction extends AbstractAction
{
    /**
     * @param Request $request
     * @return Response
     * @throws BadRequestException
     * @throws NotFoundException
     */
    public function process(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        if (
            !array_key_exists('code', $data)
            || !array_key_exists('voice', $data)
            || !array_key_exists('uid', $data)
            || !array_key_exists('name', $data)
        ) {
            throw  new BadRequestException();
        }

        /** @var Pull $pull */
        $pull = $this->entityManager->getRepository(Pull::class)->findOneBy(['code' => $data['code']]);
        if ($pull === null) {
            throw new NotFoundException();
        }

        $this->checkVoiceExists((int)$pull->getId(), (string)$data['uid']);

        $answer = $this->checkVoiceMatchAnswer((int)$data['voice'], $pull->getAnswers()->toArray());

        $voice = new Voice();
        $voice->setUid((string)$data['uid']);
        $voice->setName((string)$data['name']);
        $voice->setAnswer($answer);

        $this->entityManager->persist($voice);
        $this->entityManager->flush();

        return new Response(json_encode(['data' => $voice->getId()]), Response::HTTP_OK);
    }

    private function checkVoiceExists(int $pullId, string $uid)
    {
        $voices = $this->entityManager->getRepository(Voice::class)->findBy([
            'pull' => $pullId,
            'uid' => $uid,
        ]);

        if (!empty($voices)) {
            throw new BadRequestException('Your voice is already there.');
        }
    }

    private function checkVoiceMatchAnswer(int $voice, array $answers): Answer
    {
        $answer = array_pop(array_filter($answers, function (Answer $answer) use ($voice) {
            return $answer->getId() == $voice;
        }));

        if ($answer === null) {
            throw new NotFoundException();
        }

        return $answer;
    }
}