<?php
declare(strict_types=1);

namespace App\Action;

use App\Entity\Pull;
use App\Exception\BadRequestException;
use App\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ViewPullAction extends AbstractAction
{
    public function process(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        if (!array_key_exists('code', $data)) {
            throw  new BadRequestException();
        }

        $pull = $this->entityManager->getRepository(Pull::class)->findOneBy(['code' => $data['code']]);

        if ($pull === null) {
            throw new NotFoundException();
        }

        /** @var Pull $pull */
        return new Response(json_encode(['data' => $pull->normalize()]), Response::HTTP_OK);
    }
}