<?php
declare(strict_types=1);

namespace App\Action;

use App\Entity\Answer;
use App\Entity\Pull;
use App\Exception\BadRequestException;
use App\Exception\InvalidDataException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreatePullAction extends AbstractAction
{
    /**
     * @param Request $request
     * @return Response
     * @throws BadRequestException
     * @throws InvalidDataException
     */
    public function process(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        if (
            !array_key_exists('question', $data) ||
            !array_key_exists('answers', $data) ||
            !is_array($data['answers'])) {
            throw new BadRequestException();
        }

        $pull = $this->createAndLoad($data);

        $this->entityManager->persist($pull);
        $this->entityManager->flush();

        return new Response(json_encode(['data' => $pull->getCode()]), Response::HTTP_OK);
    }

    /**
     * @param array $data
     * @return Pull
     * @throws InvalidDataException
     */
    private function createAndLoad(array $data): Pull
    {
        $pull = new Pull();
        $pull->setQuestion($data['question']);

        if (empty($pull->getQuestion())) {
            throw new InvalidDataException();
        }

        $answers = array_unique(array_filter($data['answers'], function ($answer) {
            return (bool) $answer;
        }));

        if (count($answers) < 2) {
            throw new InvalidDataException();
        }

        foreach ($answers as $answerText) {
            $answer = new Answer();
            $answer->setText($answerText);
            $answer->setPull($pull);
        }

        $pull->setCodeValue();
        return $pull;
    }
}