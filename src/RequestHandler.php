<?php
declare(strict_types=1);

namespace App;

use App\Action\AbstractAction;
use App\Exception\BadRequestException;
use App\Exception\InvalidDataException;
use App\Exception\NotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler
{
    /** @var EntityManagerInterface */
    private $entityManager;
    private $rootDir;
    private $booted;

    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
        $this->booted = false;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @throws ORMException
     */
    public function boot(EntityManagerInterface $entityManager = null)
    {
        if (!$this->booted) {
            // todo with container
            $this->entityManager = $entityManager ?
                $entityManager :
                EntityManager::create(
                ['driver' => 'pdo_sqlite', 'path' => $this->rootDir . $_ENV['APP_DB']],
                Setup::createAnnotationMetadataConfiguration([$this->rootDir.'/src/Entity'], $_ENV['APP_ENV'] === 'dev')
            );

            $this->booted = true;
        }
    }

    public function handle(Request $request): Response
    {
        try {
            $this->boot();
            $action = $this->resolveAction($request);
            $response = $action->process($request);

            // todo return from action data and encode and response here
            return $response;
        }
        catch (NotFoundException $exception) {
            return new Response(json_encode(['error' => $exception->getMessage()]), Response::HTTP_NOT_FOUND);
        }
        catch (BadRequestException $exception) {
            return new Response(json_encode(['error' => $exception->getMessage()]), Response::HTTP_BAD_REQUEST);
        }
        catch (InvalidDataException $exception) {
            return new Response(json_encode(['error' => $exception->getMessage()]), Response::HTTP_BAD_REQUEST);
        }
        catch (\Exception $exception) {
            $message = $_ENV['APP_ENV'] === 'dev' ? $exception->getMessage() . $exception->getTraceAsString() : 'Server error';
            return new Response(json_encode(['error' => $message]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function resolveAction(Request $request): AbstractAction
    {
        $action = $request->get('action');

        if ($action === null) {
            throw new BadRequestException();
        }

        $actionClassName = 'App\\Action\\' . ucfirst($action) . 'Action';

        if (!class_exists($actionClassName)) {
            throw new BadRequestException();
        }

        // todo inject container
        return new $actionClassName($this->entityManager);
    }
}