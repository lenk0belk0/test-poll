<?php
declare(strict_types=1);

namespace App\Entity;

use App\Helper\StringHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

/**
 * @Entity()
 * @Table(name="pulls")
 **/
class Pull
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", length=32, unique=true)
     */
    protected $code;

    /**
     * @Column(type="string") *
     */
    protected $question;

    /**
     * @OneToMany(targetEntity="Answer", mappedBy="pull", cascade={"persist"})
     **/
    protected $answers;

    /**
     * @OneToMany(targetEntity="Voice", mappedBy="pull")
     **/
    protected $voices;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
        $this->voices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function setCodeValue()
    {
        $code = StringHelper::generateUid(32);
        $this->setCode($code);
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    public function addAnswer(Answer $answer): void
    {
        $this->answers[] = $answer;
    }

    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    /**
     * @return mixed
     */
    public function getVoices(): ?Collection
    {
        return $this->voices;
    }

    /**
     * @param mixed $voices
     */
    public function addVoice($voices): void
    {
        $this->voices[] = $voices;
    }

    public function normalize(): array
    {
        $result = [];
        $result['id'] = $this->getId();
        $result['code'] = $this->getCode();
        $result['question'] = $this->getQuestion();

        /** @var Answer $answer */
        foreach ($this->getAnswers() as $answer) {
            $result['answers'][] = [
                'id' => $answer->getId(),
                'text' => $answer->getText(),
                'voices' => $answer->getVoices()->count(),
            ];
        }

        $result['voices'] = $this->getVoices()->count();

        return $result;
    }
}