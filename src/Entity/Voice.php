<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

/**
 * @Entity()
 * @Table(name="voices")
 **/
class Voice
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Pull", inversedBy="voices")
     **/
    protected $pull;

    /**
     * @ManyToOne(targetEntity="Answer", inversedBy="voices")
     **/
    protected $answer;

    /** @Column(type="string") **/
    protected $uid;

    /** @Column(type="string") **/
    protected $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPull(): ?Pull
    {
        return $this->pull;
    }

    public function getAnswer(): ?Answer
    {
        return $this->answer;
    }

    public function setAnswer(Answer $answer): void
    {
        $pull = $answer->getPull();

        $answer->addVoice($this);
        $pull->addVoice($this);

        $this->answer = $answer;
        $this->pull = $pull;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }


}