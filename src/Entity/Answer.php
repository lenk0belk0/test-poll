<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;


/**
 * @Entity()
 * @Table(name="answers")
 **/
class Answer
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $text;

    /**
     * @ManyToOne(targetEntity="Pull", inversedBy="answers")
     **/
    protected $pull;

    /**
     * @OneToMany(targetEntity="Voice", mappedBy="answer")
     **/
    protected $voices;

    public function __construct()
    {
        $this->voices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText($text): void
    {
        $this->text = $text;
    }

    public function getPull(): ?Pull
    {
        return $this->pull;
    }

    public function setPull(?Pull $pull): void
    {
        $pull->addAnswer($this);
        $this->pull = $pull;
    }

    public function getVoices(): Collection
    {
        return $this->voices;
    }

    public function addVoice(Voice $voice): void
    {
        $this->voices[] = $voice;
    }
}