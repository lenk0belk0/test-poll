<?php
declare(strict_types=1);

namespace App\Helper;

class StringHelper
{
    CONST POOL = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public static function generateUid(int $length): string {
        $str = '';
        for ($i=0; $i < $length; $i++)
        {
            $str .= substr(static::POOL, mt_rand(0, strlen(static::POOL) -1), 1);
        }
        return $str;
    }
}