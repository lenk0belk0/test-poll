# What
web application for create polls, vote and view results

## requirements
- docker
- docker-compose

or

- apache2.4
- php7.2
- composer

## start with docker
copy and rename .env.example file to .env

at localhost:80 run site:
~~~
docker-compose build
docker-compose up -d
~~~
enter to container:
~~~
docker-compose exec pull_web bash
~~~
and install vendors:
~~~
composer install
~~~
## start without docker
copy and rename .env.example file to .env

change parameters in .env

direct apache to 'public' directory, allow rewrite

install vendors
~~~
composer install
~~~
## other
env, host and db connection settings in .env file

commands:

update db scheme
~~~
vendor/bin/doctrine orm:schema-tool:update --force --dump-sql
~~~
run tests
~~~
vendor/bin/phpunit
~~~