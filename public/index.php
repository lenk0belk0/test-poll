<?php
declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';
$env = Dotenv\Dotenv::create(__DIR__ . '/../');
$env->load();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Pull Service</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/assets/app.css">
    <script lang="js">
        var $_host = '<?= $_ENV['APP_HOST'] ?>';
        var $_api = $_host + '/api.php';
        var $_env = '<?= $_ENV['APP_ENV'] ?>';
    </script>
</head>
<body>
<div id="app">
    <h1 class="title"><a href="<?= $_ENV['APP_HOST'] ?>">Pull service</a></h1>
    <router-view></router-view>
</div>
<script src="/assets/vue/vue<?= $_ENV['APP_ENV'] === 'prod'? '.min': '' ?>.js"></script>
<script src="/assets/vue/vue-router<?= $_ENV['APP_ENV'] === 'prod'? '.min': '' ?>.js"></script>
<script type="module" src="/assets/modules/pull-form.js"></script>
<script type="module" src="/assets/modules/pull-view.js"></script>
<script type="module" src="/assets/app.js"></script>
</body>
</html>