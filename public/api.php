<?php
declare(strict_types=1);

use App\RequestHandler;
use Symfony\Component\HttpFoundation\Request;

$rootDir = __DIR__ . DIRECTORY_SEPARATOR . '..';
require_once $rootDir . '/vendor/autoload.php';

$env = Dotenv\Dotenv::create($rootDir);
$env->load();

$request = Request::createFromGlobals();

$requestHandler  = new RequestHandler($rootDir);
$response = $requestHandler->handle($request);

$response->send();