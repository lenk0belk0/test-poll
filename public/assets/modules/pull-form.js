export default Vue.component('pull-form', {
    data: function () {
        return {
            pull: {
                question: '',
                answers: ['', ''],
            },
            errors: [],
            submitting: false
        }
    },
    computed: {
        hasErrors: function () {
            return !!this.errors.length
        }
    },
    methods: {
        addAnswer: function () {
            this.pull.answers.push('');
        },
        startPull: function () {
            var $this = this;

            if (this.checkPull()) {
                $this.submitting = true;

                fetch($_api + '?action=createPull', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8'
                    },
                    body: JSON.stringify($this.pull)
                })
                .then(function (response) {
                    $this.submitting = false;
                    try {
                        return response.json();
                    }
                    catch (e) {
                        throw new Error('error');
                    }
                })
                .then(function (json) {
                    if (json.error === undefined) {
                        $this.$router.push({name: 'view', params: {code: json.data}});
                    }
                    else {
                        $this.errors.push(json.error)
                    }
                })
                .catch(function (error) {
                    $this.errors.push(error)
                })
            }
        },
        checkPull: function () {
            this.errors = [];

            if (!this.pull.question) {
                this.errors.push('Question cannot be empty')
            }

            var minAnswersCount = 2;
            if (this.pull.answers.filter(function (answer) {
                return !!answer
            }).length < minAnswersCount) {
                this.errors.push('Should be ' + minAnswersCount + ' or more not empty answers')
            }

            return !this.errors.length;
        }
    },
    template: '\
    <div>\
        <h2 class="subtitle">Create pull</h2>\
        <form class="pull-form">\
            <div class="error-box" v-if="hasErrors">\
                <p class="error" v-for="(error, index) in errors">\
                    {{ error }}\
                </p>\
            </div>\
            <div class="form-group">\
                <label>QUESTION</label><input v-model="pull.question" />\
            </div>\
            <div class="form-group" v-for="(answer, index) in pull.answers">\
                <label>ANSWER {{ index}}</label><input v-model="pull.answers[index]"  />\
            </div>\
            <hr />\
            <button class="button" v-on:click="addAnswer">add answer</button>\
            <hr />\
            <button class="button button--block" v-on:click="startPull" v-bind:disabled="submitting">start</button>\
        </form>\
	</div>'
})