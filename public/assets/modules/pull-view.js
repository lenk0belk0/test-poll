function generateUID(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export default Vue.component('pull-view', {
    data: function () {
        return {
            pull: {
                question: '',
                answers: [],
                voices: 0
            },
            voice: null,
            name: '',
            uid: null,
            errors: [],
            submitting: false,
        }
    },
    computed: {
        hasErrors: function () {
            return !!this.errors.length
        }
    },
    created: function () {
        this.checkUser();
        this.fetch();
        this.fetchTimeout();
    },
    methods: {
        fetch: function () {
            var $this = this;
            $this.submitting = true;
            fetch($_api + '?action=viewPull', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({code: $this.$route.params.code, uid: $this.uid})
            })
            .then(function (response) {
                $this.submitting = false;
                try {
                    return response.json();
                }
                catch (e) {
                    throw new Error('error');
                }
            })
            .then(function (json) {
                if (json.error === undefined) {
                    $this.pull = json.data
                }
            })
            .catch(function (error) {
                $this.errors.push(error);
            });
        },
        fetchTimeout: function()
        {
            this.fetch();
            setTimeout(this.fetchTimeout, 5000);
        },
        vote: function () {
            var $this = this;
            if ($this.checkVoice()) {
                $this.submitting = true;
                fetch($_api + '?action=votePull', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8'
                    },
                    body: JSON.stringify({
                        code: $this.$route.params.code,
                        voice: $this.voice,
                        uid: $this.uid,
                        name: $this.name
                    })
                })
                .then(function (response) {
                    $this.submitting = false;
                    try {
                        return response.json();
                    }
                    catch (e) {
                        throw new Error('error');
                    }
                })
                .then(function (json) {
                    if (json.error === undefined) {
                        $this.fetch();
                    }
                    else {
                        $this.errors.push(json.error);
                    }
                })
                .catch(function (error) {
                    $this.errors.push(error);
                })
            }
        },
        checkVoice: function () {
            this.errors = [];

            if (!this.voice) {
                this.errors.push('Check your voice')
            }

            if (!this.name) {
                this.errors.push('Enter your name')
            }

            return !this.errors.length;
        },
        checkUser: function () {
            if (localStorage.getItem('uid')) {
                this.uid = localStorage.getItem('uid')
            }
            else {
                this.uid = generateUID(32);
                localStorage.setItem('uid', this.uid)
            }
        },
        calcVoicesPercent: function (forAnswerCount, forPullCount) {
            var result = 0;
            if (forPullCount > 0) {
                result = Math.round(100*(forAnswerCount/forPullCount));
            }

            return result;
        }
    },
    template: '\
    <div>\
        <h2 class="subtitle">View pull</h2>\
        <form class="pull-form" v-if="pull.question">\
            <h3>{{ pull.question }}</h3>\
            <div class="error-box" v-if="hasErrors">\
                <p class="error" v-for="error in errors">\
                    {{ error }}\
                </p>\
            </div>\
            <div class="form-group form-group--radio" v-for="answer in pull.answers">\
                <input id="pull-answer-" v-bind:id="answer.id" type="radio" v-model="voice" v-bind:value="answer.id"/><label for="pull-answer-" v-bind:for="answer.id">{{ answer.text }}</label>\
            </div>\
            <div class="form-group">\
                <label>Your name</label><input type="text" v-model="name">\
            </div>\
            <hr />\
            <button class="button button--block" v-on:click="vote">vote</button>\
            <h3>Results</h3>\
            <p>Voices count: {{ pull.voices }}</p>\
            <div class="answer-voices" v-for="answer in pull.answers">\
                <div class="text">\
                    {{ answer.text }}\
                </div><div class="percents">\
                    {{ calcVoicesPercent(answer.voices, pull.voices) }}%\
                </div>\
            </div>\
        </form>\
        <div class="message-box" v-else >\
            <div v-if="submitting">loading...</div>\
            <div v-else>not found</div>\
        </div>\
    </div>'
})