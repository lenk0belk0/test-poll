import PullForm from './modules/pull-form.js';
import PullView from './modules/pull-view.js';

var routes = [
    { path: '/', name: 'create', component: PullForm },
    { path: '/view/:code', name: 'view', component: PullView },
];

var router = new VueRouter({
	routes: routes
});

var app = new Vue({
    router: router
}).$mount('#app');