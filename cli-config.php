<?php
declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once __DIR__ . '/vendor/autoload.php';

$env = Dotenv\Dotenv::create(__DIR__ );
$env->load();

$config = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/Entity"), $_ENV['APP_ENV'] === 'dev');
$connection = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . DIRECTORY_SEPARATOR . $_ENV['APP_DB'],
);

$entityManager = EntityManager::create($connection, $config);;

return ConsoleRunner::createHelperSet($entityManager);
