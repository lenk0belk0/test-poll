<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Helper\StringHelper;

class StringHelperTest extends TestCase
{

    public function testGenerateUid()
    {
        $length = 12;
        $uid = StringHelper::generateUid($length);

        $this->assertSame($length, mb_strlen($uid, 'UTF-8'));
    }

}