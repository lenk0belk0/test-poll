<?php
declare(strict_types=1);

namespace App\Tests\Action;

use App\Action\AbstractAction;
use App\Action\ViewPullAction;
use App\Entity\Pull;
use App\Exception\BadRequestException;
use App\Exception\NotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ViewPullActionTest extends TestCase
{
    public function testCreate(): ViewPullAction
    {
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        /** @var EntityManagerInterface $entityManager */
        $action = new ViewPullAction($entityManager);

        $this->assertTrue($action instanceof AbstractAction);
        return $action;
    }

    /**
     * @depends testCreate
     *
     * @param ViewPullAction $action
     * @throws BadRequestException
     * @throws NotFoundException
     */
    public function testProcessInvalid(ViewPullAction $action): void
    {
        $this->expectException(BadRequestException::class);

        $request = Request::create('', 'GET', [], [], [], [], json_encode([]));
        $action->process($request);
    }

    /**
     * @throws BadRequestException
     * @throws NotFoundException
     */
    public function testProcessNotFound(): void
    {
        $this->expectException(NotFoundException::class);

        $request = Request::create('', 'GET', [], [], [], [], json_encode(['code' => '']));
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $repository = $this->getMockBuilder(EntityRepository::class)->disableOriginalConstructor()->getMock();
        $entityManager->method('getRepository')->with(Pull::class)->willReturn($repository);

        /** @var EntityManagerInterface $entityManager */
        $action = new ViewPullAction($entityManager);

        $action->process($request);
    }

}