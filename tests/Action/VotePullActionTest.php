<?php
declare(strict_types=1);

namespace App\Tests\Action;

use App\Action\AbstractAction;
use App\Action\VotePullAction;
use App\Entity\Answer;
use App\Entity\Pull;
use App\Entity\Voice;
use App\Exception\BadRequestException;
use App\Exception\NotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class VotePullActionTest extends TestCase
{
    public function testCreate(): VotePullAction
    {
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        /** @var EntityManagerInterface $entityManager */
        $action = new VotePullAction($entityManager);

        $this->assertTrue($action instanceof AbstractAction);
        return $action;
    }

    /**
     * @dataProvider invalidData
     *
     * @param array $data
     * @throws BadRequestException
     * @throws NotFoundException
     */
    public function testProcessInvalid(array $data): void
    {
        $this->expectException(BadRequestException::class);

        $request = Request::create('', 'GET', [], [], [], [], json_encode($data));
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();

        /** @var EntityManagerInterface $entityManager */
        $action = new VotePullAction($entityManager);
        $action->process($request);
    }


    public function invalidData()
    {
        return [
            'empty data' => [[]],
            'empty code' => [['voice' => '', 'uid' => '', 'name' => '']],
            'empty voice' => [['code' => '', 'uid' => '', 'name' => '']],
            'empty uid' => [['code' => '', 'voice' => '', 'name' => '']],
            'empty name' => [['code' => '', 'voice' => '', 'uid' => '']],
        ];
    }

    /**
     * @throws BadRequestException
     * @throws NotFoundException
     */
    public function testProcessNotFound(): void
    {
        $this->expectException(NotFoundException::class);

        $request = Request::create('', 'GET', [], [], [], [], json_encode(['code' => '', 'voice' => '', 'uid' => '', 'name' => '']));

        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $repository = $this->getMockBuilder(EntityRepository::class)->disableOriginalConstructor()->getMock();
        $entityManager->method('getRepository')->with(Pull::class)->willReturn($repository);

        /** @var EntityManagerInterface $entityManager */
        $action = new VotePullAction($entityManager);

        $action->process($request);
    }

    public function testCheckInvalidAnswers(): void
    {
        $this->expectException(NotFoundException::class);

        $code = 1;
        $voice = 1;
        $answerID = 2;
        $pull = new Pull();
        $answer = new Answer();
        $pull->addAnswer($answer);
        $reflection = new \ReflectionClass($answer);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($answer, $answerID);

        $request = Request::create('', 'GET', [], [], [], [], json_encode(['code' => $code, 'voice' => $voice, 'uid' => '', 'name' => '']));

        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $repository = $this->getMockBuilder(EntityRepository::class)->disableOriginalConstructor()->getMock();
        $repository->method('findOneBy')->with(['code' => $code])->willReturn($pull);
        $repository->method('findBy')->willReturn([]);
        $entityManager->method('getRepository')->willReturn($repository);

        /** @var EntityManagerInterface $entityManager */
        $action = new VotePullAction($entityManager);

        $action->process($request);
    }
}