<?php
declare(strict_types=1);

namespace App\Tests\Action;

use App\Action\AbstractAction;
use App\Action\CreatePullAction;
use App\Exception\BadRequestException;
use App\Exception\InvalidDataException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreatePullActionTest extends TestCase
{
    public function testCreate(): CreatePullAction
    {
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();

        /** @var EntityManagerInterface $entityManager */
        $action = new CreatePullAction($entityManager);
        $this->assertTrue($action instanceof AbstractAction);

        return $action;
    }

    /**
     * @depends testCreate
     *
     * @param CreatePullAction $action
     * @throws BadRequestException
     * @throws InvalidDataException
     */
    public function testProcessBadRequest(CreatePullAction $action): void
    {
        $this->expectException(BadRequestException::class);

        $request = Request::create('', 'POST', [], [], [], [], json_encode([]));
        $action->process($request);
    }

    public function testProcessValid(): void
    {
        $question = 'test question';
        $answers = ['1' , '2', '3'];
        $request = Request::create('', 'POST', [], [], [], [], json_encode(['question' => $question, 'answers' => $answers]));
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();

        /** @var EntityManagerInterface $entityManager */
        $action = new CreatePullAction($entityManager);
        $response = $action->process($request);
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @dataProvider invalidData
     *
     * @param $question
     * @param $answers
     * @throws BadRequestException
     * @throws InvalidDataException
     */
    public function testProcessInValid($question, $answers): void
    {
        $this->expectException(InvalidDataException::class);

        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $request = Request::create(
            '', 'POST', [], [], [], [], json_encode(['question' => $question, 'answers' => $answers])
        );

        /** @var EntityManagerInterface $entityManager */
        $action = new CreatePullAction($entityManager);
        $action->process($request);
    }

    public function invalidData(): array
    {
        return [
            'invalid all' => ['', []],
            'invalid question' => ['', ['1', '2']],
            'invalid answers' => ['qwe', ['1']],
        ];
    }

}