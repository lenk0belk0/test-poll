<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Answer;
use App\Entity\Pull;
use App\Entity\Voice;
use PHPUnit\Framework\TestCase;

class VoiceTest extends TestCase
{
    public function testCreate()
    {
        $voice = new Voice();

        $this->assertEquals(null, $voice->getId());
    }

    public function testSetPull()
    {
        $pull = new Pull();
        $answer = new Answer();
        $answer->setPull($pull);
        $voice = new Voice();
        $voice->setAnswer($answer);

        $this->assertSame($pull, $voice->getPull());
        $this->assertSame($answer, $voice->getAnswer());
    }
}