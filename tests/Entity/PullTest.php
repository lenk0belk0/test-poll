<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Answer;
use App\Entity\Voice;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use App\Entity\Pull;

class PullTest extends TestCase
{
    public function testCreateAndFields(): void
    {
        $pull = new Pull();
        $this->assertSame(null, $pull->getId());
        $this->assertTrue($pull->getAnswers() instanceof Collection);
        $this->assertTrue($pull->getVoices() instanceof Collection);

    }

    public function testNormalize(): void
    {
        $questionText = 'test question';
        $code = '12341231234';
        $answerText1 = 'test answer';
        $answerText2 = 'test answer test';

        $pull = new Pull();
        $pull->setCode($code);
        $pull->setQuestion($questionText);

        $answer1 = new Answer();
        $answer1->setText($answerText1);
        $answer2 = new Answer();
        $answer2->setText($answerText2);

        $answer1->setPull($pull);
        $answer2->setPull($pull);

        $voice1 = new Voice();
        $voice1->setUid('123123');
        $voice1->setAnswer($answer1);

        $voice2 = new Voice();
        $voice2->setUid('asdfasdfasdf');
        $voice2->setAnswer($answer2);

        $normalizedPull = $pull->normalize();

        $this->assertSame(
            [
                'id' => null,
                'code' => $code,
                'question' => $questionText,
                'answers' => [
                    ['id' => null, 'text' => $answerText1, 'voices' => 1],
                    ['id' => null, 'text' => $answerText2, 'voices' => 1]
                ],
                'voices' => 2
            ],
            $normalizedPull
        );
    }

    public function testNormalizeWithEmptyVoices()
    {
        $pull = new Pull();

        $answer1 = new Answer();
        $answer2 = new Answer();

        $answer1->setPull($pull);
        $answer2->setPull($pull);

        $normalizedPull = $pull->normalize();

        $this->assertSame(
            [
                'id' => null,
                'code' => null,
                'question' => null,
                'answers' => [
                    ['id' => null, 'text' => null, 'voices' => 0],
                    ['id' => null, 'text' => null, 'voices' => 0]
                ],
                'voices' => 0
            ],
            $normalizedPull
        );
    }

    public function testSetCodeValue()
    {
        $pull = new Pull();
        $this->assertSame(null, $pull->getCode());

        $pull->setCodeValue();
        $this->assertTrue(is_string($pull->getCode()));
        $this->assertSame(32, mb_strlen($pull->getCode(), 'UTF-8'));
    }
}