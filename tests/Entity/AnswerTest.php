<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Answer;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class AnswerTest extends TestCase
{
    public function testCreateAndFields(): void
    {
        $answer = new Answer();

        $this->assertEquals(null, $answer->getId());
        $this->assertTrue($answer->getVoices() instanceof Collection);
    }
}