<?php
declare(strict_types=1);

namespace App\Tests\Exception;

use App\Exception\InvalidDataException;
use PHPUnit\Framework\TestCase;

class InvalidDataExceptionTest extends TestCase
{
    public function testCreate(): void
    {
        $exception = new InvalidDataException();

        $this->assertTrue($exception instanceof \Exception);
        $this->assertSame(InvalidDataException::INVALID_MESSAGE, $exception->getMessage());
    }
}