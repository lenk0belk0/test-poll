<?php
declare(strict_types=1);

namespace App\Tests\Exception;

use App\Exception\BadRequestException;
use PHPUnit\Framework\TestCase;

class BadRequestExceptionTest extends TestCase
{
    public function testCreate(): void
    {
        $exception = new BadRequestException();

        $this->assertTrue($exception instanceof \Exception);
        $this->assertSame(BadRequestException::BAD_REQUEST_MESSAGE, $exception->getMessage());
    }

    public function testCreateWithMessage()
    {
        $message = 'message';
        $exception = new BadRequestException($message);

        $this->assertSame($message, $exception->getMessage());
    }
}