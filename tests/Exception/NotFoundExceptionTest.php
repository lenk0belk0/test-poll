<?php
declare(strict_types=1);

namespace App\Tests\Exception;

use App\Exception\NotFoundException;
use PHPUnit\Framework\TestCase;

class NotFoundExceptionTest extends TestCase
{
    public function testCreate(): void
    {
        $exception = new NotFoundException();

        $this->assertTrue($exception instanceof \Exception);
        $this->assertSame(NotFoundException::NOT_FOUND_MESSAGE, $exception->getMessage());
    }
}