<?php
declare(strict_types=1);

namespace App\Tests;

use App\Entity\Pull;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\RequestHandler;

/**
 * Class RequestHandlerTest
 *
 * test all components
 */
class RequestHandlerTest extends TestCase
{
    /**
     * @throws ORMException
     */
    public function testCreate(): void
    {
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();

        $handler = new RequestHandler(__DIR__ . '..');

        /** @var EntityManagerInterface $entityManager */
        $handler->boot($entityManager);
        $this->assertTrue($handler instanceof  RequestHandler);
    }
    /**
     *
     *
     * @param Request $request
     * @param int $statusCode
     * @throws \Exception
     *
     * @dataProvider requests
     */
    public function testHandle($request, $statusCode): void
    {
        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $repository = $this->getMockBuilder(EntityRepository::class)->disableOriginalConstructor()->getMock();
        $repository->method('findOneBy')->willReturn(new Pull());
        $entityManager->method('getRepository')->with(Pull::class)->willReturn($repository);

        /** @var EntityManager $entityManager */
        $handler = new RequestHandler(__DIR__ . '..');
        $handler->boot($entityManager);
        $response = $handler->handle($request);

        $this->assertTrue($response instanceof  Response);
        $this->assertSame($statusCode, $response->getStatusCode());
    }

    public function requests(): array
    {
        return [
            'invalid empty action' => [
                Request::create(''),
                Response::HTTP_BAD_REQUEST
            ],
            'invalid action' => [
                Request::create('?action=createPull123'),
                Response::HTTP_BAD_REQUEST
            ],
            'invalid create pull action' => [
                Request::create('?action=createPull', 'POST', [], [], [], [], json_encode(['question' => 'qwerqwer', 'answers' => ['1', '1']])),
                Response::HTTP_BAD_REQUEST
            ],
            'valid create pull action' => [
                Request::create('?action=createPull', 'POST', [], [], [], [], json_encode(['question' => 'qwerqwer', 'answers' => ['1', '2']])),
                Response::HTTP_OK
            ]
            ,
            'valid view pull action' => [
                Request::create('?action=viewPull', 'GET', [], [], [], [], json_encode(['code' => '12'])),
                Response::HTTP_OK
            ],
        ];
    }
}